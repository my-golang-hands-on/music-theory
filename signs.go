package musictheory

import "errors"

// A Sign indicates an accidental as enumeration
type Sign int

const (
	// Natural is neither sharp nor flat - ♮
	Natural Sign = iota
	// Sharp indicates an sharpened note - ♯
	Sharp
	// Flat indicates an flattend note - ♭
	Flat
	// DoubleSharp indicates a note is increased by a whole step - 𝄪
	DoubleSharp
	// DoubleFlat indicates a note is decreased by a whole step - 𝄫
	DoubleFlat
)

// SignsList defines a SignsList for holding sign definitions for keys
type SignsList []struct {
	// SignType is defined as one of Natural, Sharp, Flat, ...
	SignType Sign
	// NbrOfSigns count of signs
	NbrOfSigns int
	// Signs for this count of signs
	Signs []string
}

// KeySigns is a list of signs for possible keys from 0 signs(Natural), over 1 to 6 sharps and 6 to 1 flats
var KeySigns = SignsList{
	{Natural, 0, []string{}},
	{Sharp, 1, []string{"F♯"}},
	{Sharp, 2, []string{"F♯", "C♯"}},
	{Sharp, 3, []string{"F♯", "C♯", "G♯"}},
	{Sharp, 4, []string{"F♯", "C♯", "G♯", "D♯"}},
	{Sharp, 5, []string{"F♯", "C♯", "G♯", "D♯", "A♯"}},
	{Sharp, 6, []string{"F♯", "C♯", "G♯", "D♯", "A♯", "E♯"}},
	{Flat, 6, []string{"B♭", "E♭", "A♭", "D♭", "G♭", "C♭"}},
	{Flat, 5, []string{"B♭", "E♭", "A♭", "D♭", "G♭"}},
	{Flat, 4, []string{"B♭", "E♭", "A♭", "D♭"}},
	{Flat, 3, []string{"B♭", "E♭", "A♭"}},
	{Flat, 2, []string{"B♭", "E♭"}},
	{Flat, 1, []string{"B♭"}},
}

// GetBySignAndNbr returns a list of signs found by the combination of SignType and Number of Signs
func (ks SignsList) GetBySignAndNbr(s Sign, nbr int) ([]string, error) {
	for _, v := range ks {
		if v.SignType == s && v.NbrOfSigns == nbr {
			return v.Signs, nil
		}
	}
	return nil, errors.New("GetBySignAndNbr: Combination of SignType and Number of signs not found")
}
