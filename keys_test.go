package musictheory

import (
	"fmt"
	"testing"
)

func ExampleCoFKeys() {
	for i := range CoFKeys {
		name := `Major`
		if CoFKeys[i].Scale.Name == `Aeolian` {
			name = `Minor`
		}
		fmt.Printf("Position: %d, Name: %s-%s, Signs: %v\n", CoFKeys[i].CirclePos, CoFKeys[i].Tonic, name, CoFKeys[i].Signs)
	}
	// Output:
	// Position: 0, Name: C-Major, Signs: []
	// Position: 0, Name: A-Minor, Signs: []
	// Position: 1, Name: G-Major, Signs: [F♯]
	// Position: 1, Name: E-Minor, Signs: [F♯]
	// Position: 2, Name: D-Major, Signs: [F♯ C♯]
	// Position: 2, Name: B-Minor, Signs: [F♯ C♯]
	// Position: 3, Name: A-Major, Signs: [F♯ C♯ G♯]
	// Position: 3, Name: F♯-Minor, Signs: [F♯ C♯ G♯]
	// Position: 4, Name: E-Major, Signs: [F♯ C♯ G♯ D♯]
	// Position: 4, Name: C♯-Minor, Signs: [F♯ C♯ G♯ D♯]
	// Position: 5, Name: B-Major, Signs: [F♯ C♯ G♯ D♯ A♯]
	// Position: 5, Name: G♯-Minor, Signs: [F♯ C♯ G♯ D♯ A♯]
	// Position: 6, Name: F♯-Major, Signs: [F♯ C♯ G♯ D♯ A♯ E♯]
	// Position: 6, Name: D♯-Minor, Signs: [F♯ C♯ G♯ D♯ A♯ E♯]
	// Position: 6, Name: G♭-Major, Signs: [B♭ E♭ A♭ D♭ G♭ C♭]
	// Position: 6, Name: E♭-Minor, Signs: [B♭ E♭ A♭ D♭ G♭ C♭]
	// Position: 7, Name: D♭-Major, Signs: [B♭ E♭ A♭ D♭ G♭]
	// Position: 7, Name: B♭-Minor, Signs: [B♭ E♭ A♭ D♭ G♭]
	// Position: 8, Name: A♭-Major, Signs: [B♭ E♭ A♭ D♭]
	// Position: 8, Name: F-Minor, Signs: [B♭ E♭ A♭ D♭]
	// Position: 9, Name: E♭-Major, Signs: [B♭ E♭ A♭]
	// Position: 9, Name: C-Minor, Signs: [B♭ E♭ A♭]
	// Position: 10, Name: B♭-Major, Signs: [B♭ E♭]
	// Position: 10, Name: C-Minor, Signs: [B♭ E♭]
	// Position: 11, Name: F-Major, Signs: [B♭]
	// Position: 11, Name: D-Minor, Signs: [B♭]
}

func ExampleCoFKey_GetParallel() {
	cmaj := CoFKeys[0]
	cmin := cmaj.GetParallel()
	fmt.Printf("C-Major -> Signs: %v, Notes: %v\n", cmaj.Signs, cmaj.Notes)
	fmt.Printf("C-Minor -> Signs: %v, Notes: %v", cmin.Signs, cmin.Notes)
	//Output:
	// C-Major -> Signs: [], Notes: [C C♯ D D♯ E F F♯ G G♯ A A♯ B]
	// C-Minor -> Signs: [B♭ E♭ A♭], Notes: [C D♭ D E♭ E F G♭ G A♭ A B♭ B]
}

func ExampleCoFKey_GetRelative() {
	cmaj := CoFKeys[0]
	amin := cmaj.GetRelative()
	fmt.Printf("C-Major -> Signs: %v, Notes: %v\n", cmaj.Signs, cmaj.Notes)
	fmt.Printf("A-Minor -> Signs: %v, Notes: %v", amin.Signs, amin.Notes)
	//Output:
	// C-Major -> Signs: [], Notes: [C C♯ D D♯ E F F♯ G G♯ A A♯ B]
	// A-Minor -> Signs: [], Notes: [A A♯ B C C♯ D D♯ E F F♯ G G♯]
}

func ExampleCoFKey_GetMinorScales() {
	ms := CoFKeys[0].GetMinorScales()
	fmt.Println("--- C-Major: Am -> Eb-Major: Cm ---")
	fmt.Println(ms)
	ms = CoFKeys[1].GetMinorScales()
	fmt.Println("--- A-Minor: Am -> A-Major: F#m ---")
	fmt.Println(ms)
	fmt.Println("--- G-Major: Em -> Bb-Major: Gm ---")
	ms = CoFKeys[2].GetMinorScales()
	fmt.Println(ms)
	fmt.Println("--- D-Minor: Em -> D-Major: Bm ---")
	ms = CoFKeys[25].GetMinorScales()
	fmt.Println(ms)
	// Output:
	// 	--- C-Major: Am -> Eb-Major: Cm ---
	// {{[A B C D E F G] [A B C D E F G♯] [A B C D E F♯ G♯]} {[C D E♭ F G A♭ B♭] [C D E♭ F G A♭ B] [C D E♭ F G A B]}}
	// --- A-Minor: Am -> A-Major: F#m ---
	// {{[A B C D E F G] [A B C D E F G♯] [A B C D E F♯ G♯]} {[F♯ G♯ A B C♯ D E] [F♯ G♯ A B C♯ D E♯] [F♯ G♯ A B C♯ D♯ E♯]}}
	// --- G-Major: Em -> Bb-Major: Gm ---
	// {{[E F♯ G A B C D] [E F♯ G A B C D♯] [E F♯ G A B C♯ D♯]} {[G A B♭ C D E♭ F] [G A B♭ C D E♭ F♯] [G A B♭ C D E F♯]}}
	// --- D-Minor: Em -> D-Major: Bm ---
	// {{[D E F G A B♭ C] [D E F G A B♭ C♯] [D E F G A B C♯]} {[B C♯ D E F♯ G A] [B C♯ D E F♯ G A♯] [B C♯ D E F♯ G♯ A♯]}}
}

func ExampleCoFKey_GetCMS() {
	res := CoFKeys[2].GetCMS()
	fmt.Println("Major-Key G")
	fmt.Println(res.Relative)
	fmt.Println(res.Parallel)
	res = CoFKeys[3].GetCMS()
	fmt.Println("Minor-Key Em")
	fmt.Println(res.Relative)
	fmt.Println(res.Parallel)
	// Output:
	// Major-Key G
	// {[G A B C D E F♯] [A B C D E F♯ G] [B C D E F♯ G A] [C D E F♯ G A B] [D E F♯ G A B C] [E F♯ G A B C D] [F♯ G A B C D E]}
	// {[G A B C D E F♯] [G A B♭ C D E F] [G A♭ B♭ C D E♭ F] [G A B C♯ D E F♯] [G A B C D E F] [G A B♭ C D E♭ F] [G A♭ B♭ C D♭ E♭ F]}
	// Minor-Key Em
	// {[G A B C D E F♯] [A B C D E F♯ G] [B C D E F♯ G A] [C D E F♯ G A B] [D E F♯ G A B C] [E F♯ G A B C D] [F♯ G A B C D E]}
	// {[G A B C D E F♯] [G A B♭ C D E F] [G A♭ B♭ C D E♭ F] [G A B C♯ D E F♯] [G A B C D E F] [G A B♭ C D E♭ F] [G A♭ B♭ C D♭ E♭ F]}
}

func ExampleCoFKey_GetScaleInKey() {
	key := CoFKeys[2]
	scale := Scale{`Major Pentatonic`, []string{`1`, `2`, `3`, `5`, `6`}, Pentatonic}
	wrong := Scale{`Wrong`, []string{`1`, `13`}, Pentatonic}
	s, err := key.GetScaleInKey(scale)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(s)
	key = CoFKeys[3]
	s, _ = key.GetScaleInKey(scale)
	fmt.Println(s)
	key = CoFKeys[4]
	s, _ = key.GetScaleInKey(scale)
	fmt.Println(s)
	_, err = key.GetScaleInKey(wrong)
	fmt.Println(err)
	// Output:
	// {{Major Pentatonic [1 2 3 5 6] Pentatonic} [G A B D E]}
	// {{Major Pentatonic [1 2 3 5 6] Pentatonic} [G A B D E]}
	// {{Major Pentatonic [1 2 3 5 6] Pentatonic} [D E F♯ A B]}
	// GetIntervalBySymbol: symbol not found
}

func TestGetMajorKeyByPosition(t *testing.T) {
	k := getMajorKeyByPosition(23)
	if k.Tonic != `F` {
		t.Errorf("expected F but got %s", k.Tonic)
	}
	k = getMajorKeyByPosition(32)
	if k.Tonic != `C` {
		t.Errorf("expected C but got %s", k.Tonic)
	}
}

func TestScaleType_String(t *testing.T) {
	var st ScaleType
	st = 23
	res := fmt.Sprintf("%s, %s, %s, %s, %s", Heptatonic, Octatonic, Chromatic, Hexatonic, st)
	if res != `Heptatonic, Octatonic, Chromatic, Hexatonic, 23` {
		t.Errorf("expcted Heptatonic, Octatonic, Chromatic, Hexatonic, 23 but got %s ", res)
	}
}
func ExampleKeyScale_PrettyPrint() {
	s := KeyScale{
		Scale{"test", []string{`1`, `2`, `♭3`, `4`, `5`}, Pentatonic},
		[]string{`c`, `d`, `e♭`, `f`, `g`},
	}
	fmt.Println(s.PrettyPrint())
	// Output:
	// Name: **test**
	//
	// Type: **Pentatonic**
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|
	// |:-:|:-:|:-:|:-:|:-:|
	// |1|2|♭3|4|5|
	// |c|d|e♭|f|g|
}
