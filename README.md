# Learning Go With Music

[![pipeline status](https://gitlab.com/my-golang-hands-on/music-theory/badges/master/pipeline.svg)](https://gitlab.com/my-golang-hands-on/music-theory/commits/master)
[![coverage report](https://gitlab.com/my-golang-hands-on/music-theory/badges/master/coverage.svg)](https://gitlab.com/my-golang-hands-on/music-theory/commits/master)

"Hands On"-Project for learning Golang. An overview of covered topics is available in goalcoverage.md

This Package provides convenience functions and prepared data for easy creation of music-theory data.

Most of the actions can be accessed through CoF by creating it through the factory method ```GetCoF()```. CoF provides access to the keys by exposing segment, ring and key methods. For details see the [wiki-page](https://gitlab.com/my-golang-hands-on/music-theory/wikis/home) where all the keys and different views on keys are available.

Example:

```go
cof := GetCoF()
md := cof.GetMajorRing().(CoFRing).PrettyPrint()
fmt.Println(md)
```

## Package Documentation

This Package provides convenience functions and prepared data for easy creation of music-theory data.

### Variables

**ChurchModeScales** is a list of the 7 modal scales starting with Ionian

```go
var ChurchModeScales = []Scale{
    {Ionian.String(), []string{`1`, `2`, `3`, `4`, `5`, `6`, `7`}, Heptatonic},
    {Dorian.String(), []string{`1`, `2`, `♭3`, `4`, `5`, `6`, `♭7`}, Heptatonic},
    {Phygrian.String(), []string{`1`, `♭2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
    {Lydian.String(), []string{`1`, `2`, `3`, `♯4`, `5`, `6`, `7`}, Heptatonic},
    {Mixolydian.String(), []string{`1`, `2`, `3`, `4`, `5`, `6`, `♭7`}, Heptatonic},
    {Aeolian.String(), []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
    {Locrian.String(), []string{`1`, `♭2`, `♭3`, `4`, `♭5`, `♭6`, `♭7`}, Heptatonic},
}
```

**Intervals** lists all intervals as a list of Symbol Types

```go
var Intervals = ISymbols{
    {`1`, `Perfect unison`, 0},
    {`♭2`, `Minor second`, 1},
    {`2`, `Major second`, 2},
    {`♭3`, `Minor third`, 3},
    {`3`, `Major third`, 4},
    {`4`, `Perfect fourth`, 5},
    {`♯4`, `Augmented fourth`, 6},
    {`♭5`, `Diminished fifth`, 6},
    {`5`, `Perfect fifth`, 7},
    {`♯5`, `Augmented fifth`, 8},
    {`♭6`, `Minor sixth`, 8},
    {`6`, `Major sixth`, 9},
    {`♭7`, `Minor seventh`, 10},
    {`7`, `Major seventh`, 11},
    {`8`, `Perfect octave`, 12},
}
```

**KeySigns** is a list of signs for possible keys from 0 signs(Natural),
    over 1 to 6 sharps and 6 to 1 flats

```go
var KeySigns = SignsList{
    {Natural, 0, []string{}},
    {Sharp, 1, []string{"F♯"}},
    {Sharp, 2, []string{"F♯", "C♯"}},
    {Sharp, 3, []string{"F♯", "C♯", "G♯"}},
    {Sharp, 4, []string{"F♯", "C♯", "G♯", "D♯"}},
    {Sharp, 5, []string{"F♯", "C♯", "G♯", "D♯", "A♯"}},
    {Sharp, 6, []string{"F♯", "C♯", "G♯", "D♯", "A♯", "E♯"}},
    {Flat, 6, []string{"B♭", "E♭", "A♭", "D♭", "G♭", "C♭"}},
    {Flat, 5, []string{"B♭", "E♭", "A♭", "D♭", "G♭"}},
    {Flat, 4, []string{"B♭", "E♭", "A♭", "D♭"}},
    {Flat, 3, []string{"B♭", "E♭", "A♭"}},
    {Flat, 2, []string{"B♭", "E♭"}},
    {Flat, 1, []string{"B♭"}},
}
```

**MinorScales** is a list of the 3 minor scales

```go
var MinorScales = []Scale{
    {`Natural-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
    {`Harmonic-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `7`}, Heptatonic},
    {`Melodic-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `6`, `7`}, Heptatonic},
}
```

### Functions

**GetChurchModeScaleNotes** returns the notes of a given notebase for the
church mode scale indicated by the ChurchMode. The notes of the major
key are required

```go
func GetChurchModeScaleNotes(cm ChurchMode, notes []string) ([]string, error)
```

*Example:*

```go
dorian, _ := GetChurchModeScaleNotes(Dorian, CoFKeys[4].Notes)
phygrian, _ := GetChurchModeScaleNotes(Phygrian, CoFKeys[4].Notes)
fmt.Println(dorian)
fmt.Println(phygrian)
 ```

*Output:*

```bash
[E F♯ G A B C♯ D]
[F♯ G A B C♯ D E]
```

**GetCoF** is a factory to get a circle of fifths

```go
func GetCoF() ICoF
```

*Example:*

```go
cof := GetCoF()
majorRing := cof.GetMajorRing()
fmt.Println(majorRing.Name())

ringSegment := majorRing.GetKey(2)
fmt.Printf("Name: %s, Signs: %s, Notes: %v\n",ringSegment.Tonic, ringSegment.Signs,ringSegment.Scale.Notes)

majorRing = cof.GetDorianRing()
ringSegment = majorRing.GetKey(2)
fmt.Printf("Name: %s, Signs: %s, Notes: %v\n",
ringSegment.Tonic, ringSegment.Signs,ringSegment.Scale.Notes)
fmt.Println(len(majorRing.GetAllKeys()))
```

*Output:*

```bash
Major
Name: D, Signs: [F♯ C♯], Notes: [D E F♯ G A B C♯]
Name: E, Signs: [F♯ C♯], Notes: [E F♯ G A B C♯ D]
12
```

**GetMinorScale** allows to get notes for the tree minor types. The NoteType indicats if scale is in a key of the left or right side of the circle of fifths. C and Am are considered as SharpType.

```go
func GetMinorScale(mt MinorType, nt NoteType, offset int) ([]string, error)
```

*Example:*

```go
fmt.Println("--- D-Minor ---")
notes, err := GetMinorScale(NaturalMinor, FlatType, 2)
if err != nil {
    fmt.Println(err)
}
fmt.Println(notes)
notes, _ = GetMinorScale(HarmonicMinor, FlatType, 2)
fmt.Println(notes)
notes, _ = GetMinorScale(MelodicMinor, FlatType, 2)
fmt.Println(notes)
fmt.Println("--- A-Minor ---")
notes, _ = GetMinorScale(NaturalMinor, SharpType, 9)
fmt.Println(notes)
notes, _ = GetMinorScale(HarmonicMinor, SharpType, 9)
fmt.Println(notes)
notes, _ = GetMinorScale(MelodicMinor, SharpType, 9)
fmt.Println(notes)
fmt.Println("--- G♯-Minor ---")
notes, _ = GetMinorScale(NaturalMinor, SharpType, 8)
fmt.Println(notes)
notes, _ = GetMinorScale(HarmonicMinor, SharpType, 8)
fmt.Println(notes)
notes, _ = GetMinorScale(MelodicMinor, SharpType, 8)
fmt.Println(notes)
fmt.Println("--- F-Minor ---")
notes, _ = GetMinorScale(NaturalMinor, FlatType, 5)
fmt.Println(notes)
notes, _ = GetMinorScale(HarmonicMinor, FlatType, 5)
fmt.Println(notes)
notes, _ = GetMinorScale(MelodicMinor, FlatType, 5)
fmt.Println(notes)
```

*Output:*

```bash
--- D-Minor ---
[D E F G A B♭ C]
[D E F G A B♭ C♯]
[D E F G A B C♯]
--- A-Minor ---
[A B C D E F G]
[A B C D E F G♯]
[A B C D E F♯ G♯]
--- G♯-Minor ---
[G♯ A♯ B C♯ D♯ E F♯]
[G♯ A♯ B C♯ D♯ E F𝄪]
[G♯ A♯ B C♯ D♯ E♯ F𝄪]
--- F-Minor ---
[F G A♭ B♭ C D♭ E♭]
[F G A♭ B♭ C D♭ E]
[F G A♭ B♭ C D E]
```

**GetNotesFrom** returns a slice of strings of 12 notesstarting with the note given by offset. This allows to do enharmonic changes on a specific note. If sharps or flats as a base are used is determined by the NoteType t. If offset is <0 or > 12 function returns an error

```go
func GetNotesFrom(t NoteType, offset int) ([]string, error)
```

*Example:*

```go
offsetOfD := 2
dnt := SharpType
notesOfD, notOk := GetNotesFrom(dnt, offsetOfD)
if notOk != nil {
    fmt.Println("there was an error of: ", notOk)
} else {
    fmt.Println(notesOfD)
}
```

*Output:*

```bash
[D D♯ E F F♯ G G♯ A A♯ B C C♯]
```

**GetNotesFromV** accepts a visitor to modify values(single notes) before
returning the slice. If sharps or flats as a base areused is determined
by the NoteType t. If offset is <0 or > 12 functionreturns an error.

```go
func GetNotesFromV(t NoteType, offset int, vis NoteVisitor) ([]string, error)
```

*Example:*

```go
visitor := func(index int, val string) string {
    if index == 11 {
        return `C♯`
    }
    return val
}
harmonic11, _ := GetNotesFromV(FlatType, 2, visitor)
fmt.Println(harmonic11)
```

*Output:*

```bash
[D E♭ E F G♭ G A♭ A B♭ B C C♯]
```

### TYPES

**CMSListRP** holds the churchmode scales for the own or parallel key and the relative key

```go
type CMSListRP struct {
    Relative ChurchModeScaleList
    Parallel ChurchModeScaleList
}
```

**ChurchMode** is an enumeration of the the church modes

```go
type ChurchMode int

const (
    // Ionian is the first churchmode
    Ionian ChurchMode = iota
    // Dorian is the second churchmode
    Dorian
    // Phygrian is the third churchmode
    Phygrian
    // Lydian is the fourth churchmode
    Lydian
    // Mixolydian is the fifth churchmode
    Mixolydian
    // Aeolian is the sixth churchmode (natural minor)
    Aeolian
    // Locrian is the seventh churchmode
    Locrian
)
```

**String** allows to print the ChurchMode enumeration value as a string

```go
func (e ChurchMode) String() string
```

**ChurchModeScaleList** is a list of all churchmode scales

```go
type ChurchModeScaleList struct {
    IonianScale     []string
    DorianScale     []string
    PhygrianScale   []string
    LydianScale     []string
    MixolydianScale []string
    AeolianScale    []string
    LocrianScale    []string
}
```

**CoFKey** represents a Circle Of Fifth Key and adds position and parallel/relative relationships to general key

```go
type CoFKey struct {
    // Key provides general key informations
    Key
    // CirclePos is the position of the key in Circle of Fifths
    CirclePos int
    // contains filtered or unexported fields
}
```

**GetCMS** returns all churchmodescales for this key and the parallel key

```go
func (k CoFKey) GetCMS() CMSListRP
```

*Example:*

```go
res := CoFKeys[2].GetCMS()
fmt.Println("Major-Key G")
fmt.Println(res.Relative)
fmt.Println(res.Parallel)
res = CoFKeys[3].GetCMS()
fmt.Println("Minor-Key Em")
fmt.Println(res.Relative)
fmt.Println(res.Parallel)
```

*Output:*

```bash
Major-Key G
{[G A B C D E F♯] [A B C D E F♯ G] [B C D E F♯ G A][C D E F♯ G A B] [D E F♯ G A B C] [E F♯ G A B C D] [F♯G A B C D E]}
{[G A B C D E F♯] [G A B♭ C D E F] [G A♭ B♭ C D E♭ F [G A B C♯ D E F♯] [G A B C D E F] [G A B♭ C D E♭ F] [GA♭ B♭ C D♭ E♭ F]}
Minor-Key Em
{[G A B C D E F♯] [A B C D E F♯ G] [B C D E F♯ G A][C D E F♯ G A B] [D E F♯ G A B C] [E F♯ G A B C D] [F♯G A B C D E]}
{[G A B C D E F♯] [G A B♭ C D E F] [G A♭ B♭ C D E♭ F [G A B C♯ D E F♯] [G A B C D E F] [G A B♭ C D E♭ F] [GA♭ B♭ C D♭ E♭ F]}
```

*GetMinorScales* returns all minorscales for that key and the parallel key

```go
func (k CoFKey) GetMinorScales() MinorListRP
```

*Example:*

```go
ms := CoFKeys[0].GetMinorScales()
fmt.Println("--- C-Major: Am -> Eb-Major: Cm ---")
fmt.Println(ms)
ms = CoFKeys[1].GetMinorScales()
fmt.Println("--- A-Minor: Am -> A-Major: F#m ---")
fmt.Println(ms)
fmt.Println("--- G-Major: Em -> Bb-Major: Gm ---")
ms = CoFKeys[2].GetMinorScales()
fmt.Println(ms)
fmt.Println("--- D-Minor: Em -> D-Major: Bm ---")
ms = CoFKeys[25].GetMinorScales()
fmt.Println(ms)
```

*Output:*

```bash
--- C-Major: Am -> Eb-Major: Cm ---
{{[A B C D E F G] [A B C D E F G♯] [A B C D E F♯ G♯]} {[C D E♭ F G A♭ B♭] [C D E♭ F G A♭ B] [C D E♭ F G A B]}}
--- A-Minor: Am -> A-Major: F#m ---
{{[A B C D E F G] [A B C D E F G♯] [A B C D E F♯ G♯]} {[F♯ G♯ A B C♯ D E] [F♯ G♯ A B C♯ D E♯] [F♯ G♯ A B C♯ D♯ E♯]}}
--- G-Major: Em -> Bb-Major: Gm ---
{{[E F♯ G A B C D] [E F♯ G A B C D♯] [E F♯ G A B C♯ D♯]} {[G A B♭ C D E♭ F] [G A B♭ C D E♭ F♯] [G A B♭ C D E F♯]}}
--- D-Minor: Em -> D-Major: Bm ---
{{[D E F G A B♭ C] [D E F G A B♭ C♯] [D E F G A B C♯]} {[B C♯ D E F♯ G A] [B C♯ D E F♯ G A♯] [B C♯ D E F♯ G♯ A♯]}}
```

**GetParallel** returns the key with the same tonic but opposite tonality (e.g. for C-Major C-Minor is returned)

```go
func (k CoFKey) GetParallel() Key
```

*Example:*

```go
cmaj := CoFKeys[0]
cmin := cmaj.GetParallel()
fmt.Printf("C-Major -> Signs: %v, Notes: %v\n", cmaj.Signs, cmaj.Notes)
fmt.Printf("C-Minor -> Signs: %v, Notes: %v", cmin.Signs, cmin.Notes)
```

*Output:*

```bash
C-Major -> Signs: [], Notes: [C C♯ D D♯ E F F♯ G G♯ A A♯ B]
C-Minor -> Signs: [B♭ E♭ A♭], Notes: [C D♭ D E♭ E F G♭ G A♭ A B♭ B]
```

**GetRelative** returns the relative key (in case of major the minor key and vice versa)

```go
func (k CoFKey) GetRelative() Key
```

*Example:*

```go
cmaj := CoFKeys[0]
amin := cmaj.GetRelative()
fmt.Printf("C-Major -> Signs: %v, Notes: %v\n", cmaj.Signs, cmaj.Notes)
fmt.Printf("A-Minor -> Signs: %v, Notes: %v", amin.Signs, amin.Notes)
```

*Output:*

```bash
C-Major -> Signs: [], Notes: [C C♯ D D♯ E F F♯ G G♯ A A♯ B]
A-Minor -> Signs: [], Notes: [A A♯ B C C♯ D D♯ E F F♯ G G♯]
```

**GetScaleInKey** returns for a given scale (formula) the corresponding notes

```go
func (k CoFKey) GetScaleInKey(s Scale) (KeyScale, error)
```

*Example:*

```go
key := CoFKeys[2]
scale := Scale{`Major Pentatonic`, []string{`1`, `2`, `3`, `5`, `6`}, Pentatonic}
wrong := Scale{`Wrong`, []string{`1`, `13`}, Pentatonic}
s, err := key.GetScaleInKey(scale)
if err != nil {
    fmt.Println(err)
}
fmt.Println(s)
key = CoFKeys[3]
s, _ = key.GetScaleInKey(scale)
fmt.Println(s)
key = CoFKeys[4]
s, _ = key.GetScaleInKey(scale)
fmt.Println(s)
_, err = key.GetScaleInKey(wrong)
fmt.Println(err)
```

*Output:*

```bash
{{Major Pentatonic [1 2 3 5 6] Pentatonic} [G A B D E]}
{{Major Pentatonic [1 2 3 5 6] Pentatonic} [G A B D E]}
{{Major Pentatonic [1 2 3 5 6] Pentatonic} [D E F♯ A B]}
GetIntervalBySymbol: symbol not found
```

**CoFRing** is a list of 12 keys of a cof

```go
type CoFRing struct {
    // contains filtered or unexported fields
}
```

**GetAllKeys** returns all keys of a ring

```go
func (r CoFRing) GetAllKeys() []CoFRingKey
```

**GetKey** returns a key at a specific position in a ring

```go
func (r CoFRing) GetKey(pos int) CoFRingKey
```

**Name** returns the name of the ring

```go
func (r CoFRing) Name() string
```

**PrettyPrint** mplements IPrettyPrint for a CoFring

```go
func (r CoFRing) PrettyPrint() string
```

**CoFRingKey** is a key of a specific cof ring

```go
type CoFRingKey struct {
    Tonic    string
    Position int
    Signs    []string
    Scale    KeyScale
    // contains filtered or unexported fields
}
```

**PrettyPrint** implements Interface IPrettyPrint for CoFRingKey

```go
func (crk CoFRingKey) PrettyPrint() string
```

**CoFSegment** defines a circle segment (position) and lists the different  scales (and tonalities) for a segment

```go
type CoFSegment struct {
    Position int
    // contains filtered or unexported fields
}
```

**GetAllKeys** returns all keys in the segment

```go
func (cs CoFSegment) GetAllKeys() []CoFRingKey
```

**GetKey** returns a key of a ringpart in the segment

```go
func (cs CoFSegment) GetKey(pos int) CoFRingKey
```

**GetKeyCount** return nbr of keys in segment

```go
func (cs CoFSegment) GetKeyCount() int
```

**GetSegmentType** returns the segment type of the segment

```go
func (cs CoFSegment) GetSegmentType() string
```

**PrettyPrint** implements IPrettyPrint for a CoFSegment

```go
func (cs CoFSegment) PrettyPrint() string
```

**ICoF** defines methods to be implemented by a cof

```go
type ICoF interface {
    GetMajorRing() ICoFRing
    GetMinorRing() ICoFRing
    GetDorianRing() ICoFRing
    GetPhygrianRing() ICoFRing
    GetLydianRing() ICoFRing
    GetMixolydianRing() ICoFRing
    GetLocrianRing() ICoFRing
    GetSegment(pos int, t SegmentType) ICoFSegment
}
```

**ICoFRing** defines methods to be implemented by a cof ring

```go
type ICoFRing interface {
    GetAllKeys() []CoFRingKey
    GetKey(pos int) CoFRingKey
    Name() string
}
```

**ICoFSegment** describes necessary methods to access a cof segment

```go
type ICoFSegment interface {
    GetAllKeys() []CoFRingKey
    GetKey(ring int) CoFRingKey
    GetKeyCount() int
    GetSegmentType() string
}
```

**IPrettyPrint** request a method for formatted printing

```go
type IPrettyPrint interface {
    PrettyPrint() string
}
```

**ISymbol** defines an Interval Info

```go
type ISymbol struct {
    // Symbol of the interval e.g. 1, b3, ...
    Symbol string
    // Name is the descriptive name of the interval
    Name string
    // Cipher is the position in a 12 note array the interval represents
    Cipher int
}
```

**GetSemis** returns the distance in semitones between to interval symbols

```go
func (i ISymbol) GetSemis(o string) (int, error)
```

*Example:*

```go
s, _ := Intervals[4].GetSemis(Intervals[6].Symbol)
fmt.Printf("the distance between %s and %s is %d semitones", Intervals[4].Symbol, Intervals[6].Symbol, s)
```

*Output:*

```bash
the distance between 3 and ♯4 is 2 semitones
```

**ISymbols** represents a type to store a list of Intervals

```go
type ISymbols []ISymbol
```

**GetIntervalByCipher** returns a Symbol struct from the Intervals List corresponding to the given cypher If the symbol can not be found an  error is returned

```go
func (s ISymbols) GetIntervalByCipher(c int) (ISymbol, error)
```

*Example:*

```go
interval, _ := Intervals.GetIntervalByCipher(11)
fmt.Println(interval)
```

*Output:*

```bash
{7 Major seventh 11}
```

**GetIntervalBySymbol** returns a Symbol struct from the Intervals List corresponding to the given symbol-string. If the symbol can not be found an error is returned

```go
func (s ISymbols) GetIntervalBySymbol(sym string) (ISymbol, error)
```

*Example:*

```go
interval, _ := Intervals.GetIntervalBySymbol(`♭3`)
fmt.Println(interval)
```

*Output:*

```bash
{♭3 Minor third 3}
```

**Key** represents a Key

```go
type Key struct {
    // Tonic note of the Key
    Tonic string
    // SignType indicates the note base sharp or flat
    SignType Sign
    // Signs lists the signs of this key
    Signs []string
    // TonalType Major or Minor
    TonalType Tonality
    // Notes is the notebase (12 notes) of that key
    Notes []string
    // NoteType sharp or flat
    NoteType NoteType
    // Scale is the scale assigned for this key (normally a mahor or a minor scale)
    Scale Scale
    // contains filtered or unexported fields
}
```

**KeyScale** extends Scale with Notes

```go
type KeyScale struct {
    Scale
    Notes []string
}
```

**PrettyPrint** implements the IPrettyPrint interface for KeyScale

```go
func (ks KeyScale) PrettyPrint() string
```

**MinorList** stores the 3 minor types

```go
type MinorList struct {
    Natural  []string
    Harmonic []string
    Melodic  []string
}
```

**MinorListRP** stores MinorLists for relative and parallel key

```go
type MinorListRP struct {
    Relative MinorList
    Parallel MinorList
}
```

**MinorType** is an enumeration for the three minor types

```go
type MinorType int

const (
    // NaturalMinor is identical to Aeolian
    NaturalMinor MinorType = iota
    // HarmonicMinor is NaturalMinor with an hightened seven
    HarmonicMinor
    // MelodicMinor is HarmonicMinor with hightened six
    MelodicMinor
)
```

```go
func (mt MinorType) String() string
```

**NoteType** is the base type for the enumeration Sharp, Flat

```go
type NoteType int

const (
    // SharpType is used for notes where sharps are used
    SharpType NoteType = iota
    // FlatType is used for notes where b's are used
    FlatType
)
```

**NoteVisitor** is a Visitor function that will be called before a note is stored and the returned value is the value that will be stored in the resulting slice of the function GetNotesFromV. By this the value can be modified before returned. The function gets the index and the current discovered value

```go
type NoteVisitor func(int, string) string
```

**Scale** desribes a scale

```go
type Scale struct {
    // Name of the string
    Name string
    // Formula in Form of interval sign 1, b3, ...
    Formula []string
    // Type is the ScaleType e.g. Heptatonic of the scale
    Type ScaleType
}
```

**GetSTFormula** returns the Tone, Semitone Steps of the scale as a slice of strings

```go
func (s Scale) GetSTFormula() []string
```

*Example:*

```go
b := ChurchModeScales[0].GetSTFormula()
fmt.Println(b)
b = ChurchModeScales[5].GetSTFormula()
fmt.Println(b)
```

*Output:*

```bash
[T T S T T T S]
[T S T T S T T]
```

**GetScaleNotes** returns for a given noteset the notes that fit to the formula

```go
func (s Scale) GetScaleNotes(notes []string) ([]string, error)
```

*Example:*

```go
minor := ChurchModeScales[5]
eMinorNotes, _ := minor.GetScaleNotes(CoFKeys[3].Notes)
majPenta := Scale{`Major Pentatonic`, []string{`1`, `2`, `3`, `5`, `6`}, Pentatonic}
pentaNotes, _ := majPenta.GetScaleNotes(CoFKeys[2].Notes)
fmt.Println(eMinorNotes)
fmt.Println(pentaNotes)
```

*Output:*

```bash
[E F♯ G A B C D]
[G A B D E]
```

**ScaleType** is an enumeration of the scaletypes based on their length

```go
type ScaleType int

const (
    // Pentatonic means 5 notes
    Pentatonic ScaleType = iota + 5
    // Hexatonic means 6 notes
    Hexatonic
    // Heptatonic means 7 notes
    Heptatonic
    // Octatonic means 8 notes
    Octatonic

    // Chromatic means 12 notes
    Chromatic
)
```

```go
func (st ScaleType) String() string
```

**SegmentType** is enum of a segment type

```go
type SegmentType int

const (
    // ChurchModes is the default segment type of a segment
    ChurchModes SegmentType = iota
    // MajorMinor forces a reduced segmenet only showing major and minor
    MajorMinor
)
```

**Sign** indicates an accidental as enumeration

```go
type Sign int

const (
    // Natural is neither sharp nor flat - ♮
    Natural Sign = iota
    // Sharp indicates an sharpened note - ♯
    Sharp
    // Flat indicates an flattend note - ♭
    Flat
    // DoubleSharp indicates a note is increased by a whole step - 𝄪
    DoubleSharp
    // DoubleFlat indicates a note is decreased by a whole step - 𝄫
    DoubleFlat
)
```

**SignsList** defines a SignsList for holding sign definitions for keys

```go
type SignsList []struct {
    // SignType is defined as one of Natural, Sharp, Flat, ...
    SignType Sign
    // NbrOfSigns count of signs
    NbrOfSigns int
    // Signs for this count of signs
    Signs []string
}
```

**GetBySignAndNbr** returns a list of signs found by the combination of SignType and Number of Signs

```go
func (ks SignsList) GetBySignAndNbr(s Sign, nbr int) ([]string, error)
```

*Example:*

```go
signs, err := KeySigns.GetBySignAndNbr(Sharp, 3)
if err == nil {
    fmt.Println(signs)
}
```

*Output:*

```bash
[F♯ C♯ G♯]
```

**Step** is the base type for Step enumeration

```go
type Step int

const (
    // Semitone is the smallest interval (used here) aka halftone, halfstep
    Semitone Step = iota + 1
    // Tone consists of tow semitones aka wholetone, fullstep
    Tone
)
```

**Tonality** defines Major or Minor

```go
type Tonality int

const (
    // Major Tonality
    Major Tonality = 0
    // Minor Tonality
    Minor Tonality = 5
)
```
