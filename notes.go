package musictheory

import "errors"

// NoteType is the base type for the enumeration Sharp, Flat
type NoteType int

const (
	// SharpType is used for notes where sharps are used
	SharpType NoteType = iota
	// FlatType is used for notes where b's are used
	FlatType
)

var sharps = []string{
	"C",
	"C♯",
	"D",
	"D♯",
	"E",
	"F",
	"F♯",
	"G",
	"G♯",
	"A",
	"A♯",
	"B",
	"C",
	"C♯",
	"D",
	"D♯",
	"E",
	"F",
	"F♯",
	"G",
	"G♯",
	"A",
	"A♯",
	"B",
}

var flats = []string{
	"C",
	"D♭",
	"D",
	"E♭",
	"E",
	"F",
	"G♭",
	"G",
	"A♭",
	"A",
	"B♭",
	"B",
	"C",
	"D♭",
	"D",
	"E♭",
	"E",
	"F",
	"G♭",
	"G",
	"A♭",
	"A",
	"B♭",
	"B",
}

// NoteVisitor is a Visitor function that will be called before a note is stored and the returned value is the value that will be stored in the resulting slice of the function GetNotesFromV. By this the value can be modified before returned.
// The function gets the index and the current discovered value
type NoteVisitor func(int, string) string

// GetNotesFromV accepts a visitor to modify values (single notes) before returning the slice.
// If sharps or flats as a base are used is determined by the NoteType t. If offset is <0 or > 12 function
// returns an error
func GetNotesFromV(t NoteType, offset int, vis NoteVisitor) ([]string, error) {
	return getNotesFromV(t, offset, vis)
}

// GetNotesFrom returns a slice of strings of 12 notes starting with the note given by offset.
// This allows to do enharmonic changes on a specific note.
// If sharps or flats as a base are used is determined by the NoteType t. If offset is <0 or > 12 function
// returns an error
func GetNotesFrom(t NoteType, offset int) ([]string, error) {
	vis := func(i int, v string) string {
		return v
	}
	return getNotesFromV(t, offset, vis)
}

func getNotesFromV(t NoteType, offset int, nv NoteVisitor) ([]string, error) {
	var from int
	if offset < 0 || offset > 12 {
		return nil, errors.New("GetNotesFrom: offset can not be < 0 or > 12")
	}
	from = offset
	to := from + 12
	notes := make([]string, 12)
	var base []string
	if t == FlatType {
		base = flats
	} else {
		base = sharps
	}
	for i, n := range base[from:to] {
		notes[i] = nv(i, n)
	}
	return notes, nil
}

func getNotesFromVPanic(t NoteType, offset int, nv NoteVisitor) []string {
	res, err := getNotesFromV(t, offset, nv)
	if err != nil {
		panic(err)
	}
	return res
}
