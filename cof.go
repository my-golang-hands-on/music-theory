package musictheory

import (
	"bytes"
	"strings"
	"text/template"
)

// SegmentType is enum of a segment type
type SegmentType int

const (
	// ChurchModes is the default segment type of a segment
	ChurchModes SegmentType = iota
	// MajorMinor forces a reduced segmenet only showing major and minor
	MajorMinor
)

type coFType struct {
	Keys []CoFKey
}

var coF = coFType{
	[]CoFKey{
		CoFKeys[0],
		CoFKeys[2],
		CoFKeys[4],
		CoFKeys[6],
		CoFKeys[8],
		CoFKeys[10],
		CoFKeys[12],
		CoFKeys[16],
		CoFKeys[18],
		CoFKeys[20],
		CoFKeys[22],
		CoFKeys[24],
	},
}

// CoFRingKey is a key of a specific cof ring
type CoFRingKey struct {
	Tonic    string
	Position int
	Signs    []string
	Scale    KeyScale
	keyPtr   *CoFKey
	ringPtr  *CoFRing
}

// CoFRing is a list of 12 keys of a cof
type CoFRing struct {
	name string
	keys []CoFRingKey
}

// CoFSegment defines a circle segment (position) and lists the different scales (and tonalities) for a segment
type CoFSegment struct {
	segmentType string
	Position    int
	keys        []CoFRingKey
}

// GetKeyCount return nbr of keys in segment
func (cs CoFSegment) GetKeyCount() int {
	if cs.segmentType == `MajorMinor` {
		return 2
	}
	return len(cs.keys)
}

// GetSegmentType returns the segment type of the segment
func (cs CoFSegment) GetSegmentType() string {
	return cs.segmentType
}

func setSegmentType(st SegmentType) string {
	if st == MajorMinor {
		return `MajorMinor`
	}
	return `ChurchModes`
}

// GetKey returns a key of a ringpart in the segment
func (cs CoFSegment) GetKey(pos int) CoFRingKey {
	if pos >= cs.GetKeyCount() || pos < 0 {
		pos = 0
	}
	return cs.keys[pos]
}

// GetAllKeys returns all keys in the segment
func (cs CoFSegment) GetAllKeys() []CoFRingKey {
	if cs.segmentType == `MajorMinor` {
		return []CoFRingKey{
			cs.keys[0],
			cs.keys[5],
		}
	}
	return cs.keys
}

func isMajorMinor(cs CoFSegment) bool {
	if cs.GetSegmentType() == `MajorMinor` {
		return true
	}
	return false
}

func getMinorScaleFromKey(k Key, mt MinorType) KeyScale {
	s, err := GetMinorScale(mt, k.NoteType, k.offset+9)
	if err != nil {
		panic(err)
	}
	ks := KeyScale{
		getMinorScaleByName(mt.String()),
		s,
	}
	return ks
}

// PrettyPrint implements IPrettyPrint for a CoFSegment
func (cs CoFSegment) PrettyPrint() string {
	var tpl bytes.Buffer
	data := struct {
		Name         string
		Position     int
		Signs        string
		Keys         []CoFRingKey
		ST           string
		IsMajorMinor bool
		Header       func(i int) string
		Harmonic     KeyScale
		Melodic      KeyScale
	}{
		cs.GetKey(0).Tonic,
		cs.GetKey(0).Position,
		signs(cs.GetKey(0).Signs),
		cs.GetAllKeys(),
		cs.GetSegmentType(),
		isMajorMinor(cs),
		roman,
		getMinorScaleFromKey(cs.GetKey(0).keyPtr.Key, HarmonicMinor),
		getMinorScaleFromKey(cs.GetKey(0).keyPtr.Key, MelodicMinor),
	}
	t, err := template.New("segment").Parse(`
## Key: {{.Name}}

Position: **{{.Position }}**

Signs: **{{.Signs}}**

{{if .IsMajorMinor}}### {{.Name}}-Major

|{{range $index,$note := (index .Keys 0).Scale.Notes}}{{call $.Header $index}}|{{end}}
|{{range $index,$interval := (index .Keys 0).Scale.Formula}}:-:|{{end}}
|{{range $index,$interval := (index .Keys 0).Scale.Formula}}{{.}}|{{end}}
|{{range $index,$note := (index .Keys 0).Scale.Notes}}{{.}}|{{end}}

### {{(index .Keys 1).Tonic}}-Minor

**Natural**

|{{range $index,$note := (index .Keys 1).Scale.Notes}}{{call $.Header $index}}|{{end}}
|{{range $index,$interval := (index .Keys 1).Scale.Formula}}:-:|{{end}}
|{{range $index,$interval := (index .Keys 1).Scale.Formula}}{{.}}|{{end}}
|{{range $index,$note := (index .Keys 1).Scale.Notes}}{{.}}|{{end}}

**Harmonic**

|{{range $index,$note := .Harmonic.Notes}}{{call $.Header $index}}|{{end}}
|{{range $index,$note := .Harmonic.Notes}}:-:|{{end}}
|{{range $index,$interval := .Harmonic.Scale.Formula}}{{.}}|{{end}}
|{{range $index,$note := .Harmonic.Notes}}{{.}}|{{end}}

**Melodic**

|{{range $index,$note := .Melodic.Notes}}{{call $.Header $index}}|{{end}}
|{{range $index,$note := .Melodic.Notes}}:-:|{{end}}
|{{range $index,$interval := .Melodic.Scale.Formula}}{{.}}|{{end}}
|{{range $index,$note := .Melodic.Notes}}{{.}}|{{end}}
{{else}}{{range $index,$note := .Keys}}### {{.Tonic}}-{{.Scale.Name}}

|{{range $index,$note := (index $.Keys $index).Scale.Notes}}{{call $.Header $index}}|{{end}}
|{{range $index,$interval := (index $.Keys $index).Scale.Formula}}:-:|{{end}}
|{{range $index,$interval := (index $.Keys $index).Scale.Formula}}{{.}}|{{end}}
|{{range $index,$note := (index $.Keys $index).Scale.Notes}}{{.}}|{{end}}
{{end}}
{{end}}
	`)
	err = t.Execute(&tpl, data)
	if err != nil {
		panic(err)
	}
	return tpl.String()
}

// PrettyPrint mplements IPrettyPrint for a CoFring
func (r CoFRing) PrettyPrint() string {
	res := ""
	for _, k := range r.GetAllKeys() {
		res += k.PrettyPrint() + "\n"
	}
	return res
}

// GetKey returns a key at a specific position in a ring
func (r CoFRing) GetKey(pos int) CoFRingKey {
	if pos > 11 || pos < 0 {
		pos = 0
	}
	return r.keys[pos]
}

// GetAllKeys returns all keys of a ring
func (r CoFRing) GetAllKeys() []CoFRingKey {
	return r.keys
}

// Name returns the name of the ring
func (r CoFRing) Name() string {
	return r.name
}

// ICoFRing defines methods to be implemented by a cof ring
type ICoFRing interface {
	GetAllKeys() []CoFRingKey
	GetKey(pos int) CoFRingKey
	Name() string
}

// ICoFSegment describes necessary methods to access a cof segment
type ICoFSegment interface {
	GetAllKeys() []CoFRingKey
	GetKey(ring int) CoFRingKey
	GetKeyCount() int
	GetSegmentType() string
}

// ICoF defines methods to be implemented by a cof
type ICoF interface {
	GetMajorRing() ICoFRing
	GetMinorRing() ICoFRing
	GetDorianRing() ICoFRing
	GetPhygrianRing() ICoFRing
	GetLydianRing() ICoFRing
	GetMixolydianRing() ICoFRing
	GetLocrianRing() ICoFRing
	GetSegment(pos int, t SegmentType) ICoFSegment
}

func signs(s []string) string {
	if len(s) == 0 {
		return `♮`
	}
	return strings.Join(s, `, `)
}

// PrettyPrint implements Interface IPrettyPrint for CoFRingKey
func (crk CoFRingKey) PrettyPrint() string {
	var tpl bytes.Buffer
	data := struct {
		Name     string
		Position int
		Signs    string
		Ring     string
		Parallel Key
	}{
		crk.Tonic,
		crk.Position + 1,
		signs(crk.Signs),
		crk.ringPtr.Name(),
		crk.keyPtr.GetParallel(),
	}
	t, err := template.New("key").Parse(`
## Key: {{.Name}} ({{.Ring}})

Position: **{{.Position }}**

Signs: **{{.Signs}}**

### Scale
`)
	err = t.Execute(&tpl, data)
	if err != nil {
		panic(err)
	}
	return tpl.String() + crk.Scale.PrettyPrint()
}

// IPrettyPrint request a method for formatted printing
type IPrettyPrint interface {
	PrettyPrint() string
}

// GetCoF is a factory to get a circle of fifths
func GetCoF() ICoF {
	return circleOfFifth
}

type coFt struct{}

var circleOfFifth = coFt{}

func (c coFt) GetSegment(pos int, t SegmentType) ICoFSegment {
	if pos < 0 || pos > 11 {
		pos = 0
	}
	cs := CoFSegment{
		setSegmentType(t),
		pos,
		getSegmentKeysCM(pos, c),
	}
	return cs
}

func (c coFt) GetMajorRing() ICoFRing {
	return getRing(coF, Ionian, `Major`)
}

func (c coFt) GetMinorRing() ICoFRing {
	return getRing(coF, Aeolian, `Minor`)
}

func (c coFt) GetDorianRing() ICoFRing {
	return getRing(coF, Dorian, `Dorian`)
}

func (c coFt) GetPhygrianRing() ICoFRing {
	return getRing(coF, Phygrian, `Phygrian`)
}

func (c coFt) GetLydianRing() ICoFRing {
	return getRing(coF, Lydian, `Lydian`)
}

func (c coFt) GetMixolydianRing() ICoFRing {
	return getRing(coF, Mixolydian, `Mixolydian`)
}

func (c coFt) GetLocrianRing() ICoFRing {
	return getRing(coF, Locrian, `Locrian`)
}

func getRing(c coFType, cm ChurchMode, name string) CoFRing {
	keys := make([]CoFRingKey, 12)
	var ret CoFRing
	for i, v := range c.Keys {
		switch cm {
		case Ionian:
			keys[i] = createRingKey(v.GetCMS().Relative.IonianScale, &c.Keys[i], ChurchModeScales[0], &ret)
		case Dorian:
			keys[i] = createRingKey(v.GetCMS().Relative.DorianScale, &c.Keys[i], ChurchModeScales[1], &ret)
		case Phygrian:
			keys[i] = createRingKey(v.GetCMS().Relative.PhygrianScale, &c.Keys[i], ChurchModeScales[2], &ret)
		case Lydian:
			keys[i] = createRingKey(v.GetCMS().Relative.LydianScale, &c.Keys[i], ChurchModeScales[3], &ret)
		case Mixolydian:
			keys[i] = createRingKey(v.GetCMS().Relative.MixolydianScale, &c.Keys[i], ChurchModeScales[4], &ret)
		case Aeolian:
			keys[i] = createRingKey(v.GetCMS().Relative.AeolianScale, &c.Keys[i], ChurchModeScales[5], &ret)
		case Locrian:
			keys[i] = createRingKey(v.GetCMS().Relative.LocrianScale, &c.Keys[i], ChurchModeScales[6], &ret)
		}
	}
	ret = CoFRing{
		name: name,
		keys: keys,
	}
	return ret
}

func createRingKey(notes []string, ptr *CoFKey, s Scale, r *CoFRing) CoFRingKey {
	res := CoFRingKey{
		Tonic:    notes[0],
		Position: ptr.CirclePos,
		Signs:    ptr.Signs,
		Scale: KeyScale{
			Scale: s,
			Notes: notes,
		},
		keyPtr:  ptr,
		ringPtr: r,
	}
	return res
}

func getSegmentKeysCM(position int, c ICoF) []CoFRingKey {
	keys := make([]CoFRingKey, 7)
	keys[0] = c.GetMajorRing().GetKey(position)
	keys[1] = c.GetDorianRing().GetKey(position)
	keys[2] = c.GetPhygrianRing().GetKey(position)
	keys[3] = c.GetLydianRing().GetKey(position)
	keys[4] = c.GetMixolydianRing().GetKey(position)
	keys[5] = c.GetMinorRing().GetKey(position)
	keys[6] = c.GetLocrianRing().GetKey(position)
	return keys
}
