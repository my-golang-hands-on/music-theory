package musictheory

import (
	"fmt"
	"testing"
)

func TestGetNotesFrom(t *testing.T) {
	n, ok := GetNotesFrom(SharpType, 4)
	if ok != nil {
		t.Errorf("Expected no error got %s", ok)
	}
	if len(n) != 12 {
		t.Errorf("Expected a length of 12 and got %d", len(n))
	}
	if n[0] != `E` {
		t.Errorf("Expected E to be first note and got %s", n)
	}
	n, ok = GetNotesFrom(FlatType, 1)
	if ok != nil {
		t.Errorf("Expected no error got %s", ok)
	}
	if n[0] != `D♭` {
		t.Errorf("Expected D♭ to be first note and got %s", n)
	}
	n, ok = GetNotesFrom(FlatType, -1)
	if ok == nil {
		t.Errorf("Expected error but got no error")
	}
	n, ok = GetNotesFrom(FlatType, 99)
	if ok == nil {
		t.Errorf("Expected error but got no error")
	}
}

func ExampleGetNotesFrom() {
	offsetOfD := 2
	dnt := SharpType
	notesOfD, notOk := GetNotesFrom(dnt, offsetOfD)
	if notOk != nil {
		fmt.Println("there was an error of: ", notOk)
	} else {
		fmt.Println(notesOfD)
	}
	// Output:
	// [D D♯ E F F♯ G G♯ A A♯ B C C♯]
}

func TestGetNotesFromV(t *testing.T) {
	visitor := func(index int, val string) string {
		return "x"
	}
	notesOfd, notOk := GetNotesFromV(SharpType, 2, visitor)
	if notOk != nil {
		t.Errorf("expteced no error but got one")
	}
	if notesOfd[1] != `x` {
		t.Errorf("expected x but got %s", notesOfd[1])
	}
}

func TestGetNotesFromVPanic(t *testing.T) {
	cb := func(i int, v string) string {
		return v
	}
	func() {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("TestUserFail should have panicked!")
			}
		}()
		getNotesFromVPanic(SharpType, 99, cb)
	}()
}

func ExampleGetNotesFromV() {
	visitor := func(index int, val string) string {
		if index == 11 {
			return `C♯`
		}
		return val
	}
	harmonic11, _ := GetNotesFromV(FlatType, 2, visitor)
	fmt.Println(harmonic11)
	// Output:
	// [D E♭ E F G♭ G A♭ A B♭ B C C♯]
}
