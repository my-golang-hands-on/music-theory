package musictheory

import (
	"fmt"
	"testing"
)

func ExampleGetCoF() {
	cof := GetCoF()
	majorRing := cof.GetMajorRing()
	fmt.Println(majorRing.Name())
	ringSegment := majorRing.GetKey(2)
	fmt.Printf("Name: %s, Signs: %s, Notes: %v\n", ringSegment.Tonic, ringSegment.Signs, ringSegment.Scale.Notes)
	majorRing = cof.GetDorianRing()
	ringSegment = majorRing.GetKey(2)
	fmt.Printf("Name: %s, Signs: %s, Notes: %v\n", ringSegment.Tonic, ringSegment.Signs, ringSegment.Scale.Notes)
	fmt.Println(len(majorRing.GetAllKeys()))
	// Output:
	// Major
	// Name: D, Signs: [F♯ C♯], Notes: [D E F♯ G A B C♯]
	// Name: E, Signs: [F♯ C♯], Notes: [E F♯ G A B C♯ D]
	// 12
}

func TestGetCoF(t *testing.T) {
	cof := GetCoF()
	r := cof.GetDorianRing()
	r = cof.GetLocrianRing()
	r = cof.GetLydianRing()
	r = cof.GetMinorRing()
	r = cof.GetMixolydianRing()
	r = cof.GetPhygrianRing()
	if len(r.GetAllKeys()) != 12 {
		t.Error("len not correct")
	}
	x := cof.GetDorianRing().GetKey(14)
	if x.Tonic != `D` {
		t.Errorf("expected D but got %s", x.Tonic)
	}
}

func ExampleCoFRingKey_PrettyPrint() {
	cof := GetCoF()
	c := cof.GetDorianRing().GetKey(10)
	fmt.Println(c.PrettyPrint())
	// Output:
	// ## Key: C (Dorian)
	//
	// Position: **11**
	//
	// Signs: **B♭, E♭**
	//
	// ### Scale
	//
	// Name: **Dorian**
	//
	// Type: **Heptatonic**
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|Ⅵ|Ⅶ|
	// |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	// |1|2|♭3|4|5|6|♭7|
	// |C|D|E♭|F|G|A|B♭|
}

func ExampleICoF_GetSegment() {
	cof := GetCoF()
	seg := cof.GetSegment(0, ChurchModes)
	fmt.Println(seg.GetKeyCount())
	fmt.Println(len(seg.GetAllKeys()))
	fmt.Println(seg.GetSegmentType())
	fmt.Println("--------------")
	key := seg.GetKey(0)
	fmt.Println(key.Tonic)
	fmt.Println(key.Signs)
	fmt.Println("--------------")
	key = cof.GetSegment(2, ChurchModes).GetKey(3)
	fmt.Println(key.Tonic)
	fmt.Println(key.Signs)
	// Output:
	// 7
	// 7
	// ChurchModes
	// --------------
	// C
	// []
	// --------------
	// G
	// [F♯ C♯]
}

func ExampleCoFSegment_PrettyPrint() {
	cof := GetCoF()
	seg := cof.GetSegment(0, MajorMinor).(CoFSegment)
	md := seg.PrettyPrint()
	fmt.Println(md)

	// Output:
	// ## Key: C
	//
	// Position: **0**
	//
	// Signs: **♮**
	//
	// ### C-Major
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|Ⅵ|Ⅶ|
	// |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	// |1|2|3|4|5|6|7|
	// |C|D|E|F|G|A|B|
	//
	// ### A-Minor
	//
	// **Natural**
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|Ⅵ|Ⅶ|
	// |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	// |1|2|♭3|4|5|♭6|♭7|
	// |A|B|C|D|E|F|G|
	//
	// **Harmonic**
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|Ⅵ|Ⅶ|
	// |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	// |1|2|♭3|4|5|♭6|7|
	// |A|B|C|D|E|F|G♯|
	//
	// **Melodic**
	//
	// |Ⅰ|Ⅱ|Ⅲ|Ⅳ|Ⅴ|Ⅵ|Ⅶ|
	// |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	// |1|2|♭3|4|5|6|7|
	// |A|B|C|D|E|F♯|G♯|
}

func TestCoFRing_PrettyPrint(t *testing.T) {
	cof := GetCoF()
	l1 := len(cof.GetDorianRing().(CoFRing).PrettyPrint())
	l2 := len(cof.GetMinorRing().(CoFRing).PrettyPrint())
	if l1 < 1000 || l2 < 1000 {
		t.Errorf("output too small")
	}
}

/*
func ExampleCoFRing_PrettyPrint() {
	cof := GetCoF()
	md := cof.GetMinorRing().(CoFRing).PrettyPrint()
	fmt.Println(md)
	// Output:
	//
}
*/
