package musictheory

import (
	"fmt"
	"testing"
)

func TestISymbols_GetIntervalBySymbol(t *testing.T) {
	_, notOk := Intervals.GetIntervalBySymbol(`x`)
	if notOk == nil {
		t.Errorf("exptected error but got none")
	}
}

func TestGetSemis(t *testing.T) {
	s, e := Intervals[4].GetSemis(Intervals[6].Symbol)
	if e != nil {
		t.Errorf("expected no error but got one")
	}
	if s != 2 {
		t.Errorf("exptected 2 but got %d", s)
	}
	_, e = Intervals[4].GetSemis(`x`)
	if e == nil {
		t.Errorf("expected error but got none")
	}
}

func TestGetIntervalBySearch(t *testing.T) {
	_, e := getIntervalBySearch(Intervals, false)
	if e == nil {
		t.Errorf("expected error bot got none")
	}
}

func ExampleISymbol_GetSemis() {
	s, _ := Intervals[4].GetSemis(Intervals[6].Symbol)
	fmt.Printf("the distance between %s and %s is %d semitones", Intervals[4].Symbol, Intervals[6].Symbol, s)
	// Output:
	// the distance between 3 and ♯4 is 2 semitones
}
func ExampleISymbols_GetIntervalBySymbol() {
	interval, _ := Intervals.GetIntervalBySymbol(`♭3`)
	fmt.Println(interval)
	// Output:
	// {♭3 Minor third 3}
}

func ExampleISymbols_GetIntervalByCipher() {
	interval, _ := Intervals.GetIntervalByCipher(11)
	fmt.Println(interval)
	// Output:
	// {7 Major seventh 11}
}
