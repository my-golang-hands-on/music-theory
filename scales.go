package musictheory

import "fmt"
import "strings"

// ChurchMode is an enumeration of the the church modes
type ChurchMode int

// MinorType is an enumeration for the three minor types
type MinorType int

// ScaleType is an enumeration of the scaletypes based on their length
type ScaleType int

const (
	// Ionian is the first churchmode
	Ionian ChurchMode = iota
	// Dorian is the second churchmode
	Dorian
	// Phygrian is the third churchmode
	Phygrian
	// Lydian is the fourth churchmode
	Lydian
	// Mixolydian is the fifth churchmode
	Mixolydian
	// Aeolian is the sixth churchmode (natural minor)
	Aeolian
	// Locrian is the seventh churchmode
	Locrian
)
const (
	// NaturalMinor is identical to Aeolian
	NaturalMinor MinorType = iota
	// HarmonicMinor is NaturalMinor with an hightened seven
	HarmonicMinor
	// MelodicMinor is HarmonicMinor with hightened six
	MelodicMinor
)
const (
	// Pentatonic means 5 notes
	Pentatonic ScaleType = iota + 5
	// Hexatonic means 6 notes
	Hexatonic
	// Heptatonic means 7 notes
	Heptatonic
	// Octatonic means 8 notes
	Octatonic
	_
	_
	_
	// Chromatic means 12 notes
	Chromatic
)

// Scale desribes a scale
type Scale struct {
	// Name of the string
	Name string
	// Formula in Form of interval sign 1, b3, ...
	Formula []string
	// Type is the ScaleType e.g. Heptatonic of the scale
	Type ScaleType
}

func (mt MinorType) String() string {
	switch mt {
	case NaturalMinor:
		return `Natural-Minor`
	case HarmonicMinor:
		return `Harmonic-Minor`
	case MelodicMinor:
		return `Melodic-Minor`
	default:
		return fmt.Sprintf("%d", int(mt))
	}
}

func (st ScaleType) String() string {
	switch st {
	case Heptatonic:
		return `Heptatonic`
	case Hexatonic:
		return `Hexatonic`
	case Pentatonic:
		return `Pentatonic`
	case Octatonic:
		return `Octatonic`
	case Chromatic:
		return `Chromatic`
	default:
		return fmt.Sprintf("%d", int(st))
	}
}

// String allows to print the ChurchMode enumeration value as a string
func (e ChurchMode) String() string {
	switch e {
	case Ionian:
		return "Ionian"
	case Dorian:
		return "Dorian"
	case Phygrian:
		return "Phygrian"
	case Lydian:
		return "Lydian"
	case Mixolydian:
		return "Mixolydian"
	case Aeolian:
		return "Aeolian"
	case Locrian:
		return "Locrian"
	default:
		return fmt.Sprintf("%d", int(e))
	}
}

// ChurchModeScales is a list of the 7 modal scales starting with Ionian
var ChurchModeScales = []Scale{
	{Ionian.String(), []string{`1`, `2`, `3`, `4`, `5`, `6`, `7`}, Heptatonic},
	{Dorian.String(), []string{`1`, `2`, `♭3`, `4`, `5`, `6`, `♭7`}, Heptatonic},
	{Phygrian.String(), []string{`1`, `♭2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
	{Lydian.String(), []string{`1`, `2`, `3`, `♯4`, `5`, `6`, `7`}, Heptatonic},
	{Mixolydian.String(), []string{`1`, `2`, `3`, `4`, `5`, `6`, `♭7`}, Heptatonic},
	{Aeolian.String(), []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
	{Locrian.String(), []string{`1`, `♭2`, `♭3`, `4`, `♭5`, `♭6`, `♭7`}, Heptatonic},
}

// MinorScales is a list of the 3 minor scales
var MinorScales = []Scale{
	{`Natural-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `♭7`}, Heptatonic},
	{`Harmonic-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `♭6`, `7`}, Heptatonic},
	{`Melodic-Minor`, []string{`1`, `2`, `♭3`, `4`, `5`, `6`, `7`}, Heptatonic},
}

func getMinorScaleByName(name string) Scale {
	for _, ms := range MinorScales {
		if ms.Name == name {
			return ms
		}
	}
	return MinorScales[0]
}

// GetSTFormula returns the Tone, Semitone Steps of the scale as a slice of strings
func (s Scale) GetSTFormula() []string {
	last := int(s.Type) + 1
	base := append(s.Formula, fmt.Sprintf("%d", last))
	var ts = make([]string, len(base)-1)
	for i := range base {
		if i > 0 {
			prev, _ := Intervals.GetIntervalBySymbol(base[i-1])
			current, _ := Intervals.GetIntervalBySymbol(base[i])
			dist, _ := prev.GetSemis(current.Symbol)
			ts[i-1] = getStepString(dist)
		}
	}
	return ts
}

func getStepString(i int) string {
	switch i {
	case 1:
		return `S`
	case 2:
		return `T`
	default:
		return ``
	}
}

// GetScaleNotes returns for a given noteset the notes that fit to the formula
func (s Scale) GetScaleNotes(notes []string) ([]string, error) {
	sn := make([]string, len(s.Formula))
	for i, v := range s.Formula {
		interval, err := Intervals.GetIntervalBySymbol(v)
		if err != nil {
			return sn, err
		}
		sn[i] = notes[interval.Cipher]
	}
	return sn, nil
}

var churchModeOffsets = []int{
	0,
	2,
	4,
	5,
	7,
	9,
	11,
}

// GetChurchModeScaleNotes returns the notes of a given notebase for the church mode scale indicated by the ChurchMode. The notes of the major key are required
func GetChurchModeScaleNotes(cm ChurchMode, notes []string) ([]string, error) {
	var offset = churchModeOffsets[int(cm)]
	scale := ChurchModeScales[int(cm)]
	notes = append(notes[offset:], notes[:offset]...)
	return scale.GetScaleNotes(notes)
}

// GetMinorScale allows to get notes for the tree minor types. The NoteType indicats if scale is in a key of the left or right side of the circle of fifths. C and Am are considered as SharpType.
func GetMinorScale(mt MinorType, nt NoteType, offset int) ([]string, error) {
	if offset > 11 {
		offset = offset - 12
	}
	var scale Scale
	var spec func(index int) bool
	switch mt {
	case NaturalMinor:
		scale = MinorScales[0]
		spec = func(index int) bool {
			return false
		}
	case HarmonicMinor:
		scale = MinorScales[1]
		spec = func(index int) bool {
			if index == 11 {
				return true
			}
			return false
		}
	case MelodicMinor:
		scale = MinorScales[2]
		spec = func(index int) bool {
			if index == 11 || index == 9 {
				return true
			}
			return false
		}
	default:
		scale = MinorScales[0]
	}
	notes, err := GetNotesFromV(nt, offset, func(i int, s string) string {
		var r string
		if spec(i) {
			r = s
			if nt == FlatType {
				if len(s) > 1 && strings.Contains(s, `♭`) {
					r = getNotesFromVPanic(SharpType, offset, defaultVisitor)[i]
				}
			}
			if nt == SharpType {
				r = getNotesFromVPanic(SharpType, offset, defaultVisitor)[i-1]
				if len(r) > 1 {
					r = strings.Replace(r, `♯`, `𝄪`, 1)
				} else {
					r += `♯`
				}
			}
			return r
		}
		return s
	})
	if err != nil {
		return []string{}, err
	}
	return scale.GetScaleNotes(notes)
}
