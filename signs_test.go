package musictheory

import (
	"fmt"
	"testing"
)

func TestGetBySignAndNbr(t *testing.T) {
	n := Natural
	s, err := KeySigns.GetBySignAndNbr(n, 0)
	if err != nil {
		t.Errorf("expected not error but got %s", err)
	}
	if len(s) > 0 {
		t.Errorf("expected length 0 for Natural but got %d", len(s))
	}
	s, err = KeySigns.GetBySignAndNbr(Sharp, 3)
	if len(s) != 3 {
		t.Errorf("expected length 3 for Sharp 3 but got %d", len(s))
	}
	if s[0] != `F♯` || s[2] != `G♯` {
		t.Errorf("expected F# and G# but got %s, %s", s[0], s[3])
	}
	s, err = KeySigns.GetBySignAndNbr(Sharp, 6)
	if len(s) != 6 {
		t.Errorf("expected length 6 for Flat 6 but got %d", len(s))
	}
	s, err = KeySigns.GetBySignAndNbr(Natural, 2)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	s, err = KeySigns.GetBySignAndNbr(Sharp, 8)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	s, err = KeySigns.GetBySignAndNbr(Flat, 1)
	if s[0] != `B♭` {
		t.Errorf("expected Bb but got %s", s[0])
	}
	s, err = KeySigns.GetBySignAndNbr(DoubleFlat, 0)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	if s != nil {
		t.Errorf("expected result to be nil but but got %v", s)
	}
	s, err = KeySigns.GetBySignAndNbr(DoubleSharp, 12)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	s, err = KeySigns.GetBySignAndNbr(Sharp, 0)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	l := SignsList{
		{DoubleFlat, 2, []string{"a"}},
	}
	s, err = l.GetBySignAndNbr(DoubleFlat, 2)
	if err != nil {
		t.Errorf("expected no error but got one")
	}
	if s[0] != `a` {
		t.Errorf("expected a but got %s", s[0])
	}
	s, err = l.GetBySignAndNbr(Flat, 2)
	if err == nil {
		t.Errorf("expected error but got none")
	}
	empty := SignsList{}
	s, err = empty.GetBySignAndNbr(Flat, 9)
}

func ExampleSignsList_GetBySignAndNbr() {
	signs, err := KeySigns.GetBySignAndNbr(Sharp, 3)
	if err == nil {
		fmt.Println(signs)
	}
	// Output:
	// [F♯ C♯ G♯]
}
