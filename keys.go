// Package musictheory provides types, data and functions to support easy creation of musical explanations.
// It provides funktionality for Signs, Keys, Intervals and Scales
package musictheory

import (
	"bytes"
	"text/template"
)

// Tonality defines Major or Minor
type Tonality int

const (
	// Major Tonality
	Major Tonality = 0
	// Minor Tonality
	Minor Tonality = 5
)

// Key represents a Key
type Key struct {
	// Tonic note of the Key
	Tonic string
	// SignType indicates the note base sharp or flat
	SignType Sign
	// Signs lists the signs of this key
	Signs []string
	// TonalType Major or Minor
	TonalType Tonality
	// Notes is the notebase (12 notes) of thar key
	Notes []string
	// NoteType sharp or flat
	NoteType NoteType
	// Scale is the scale assigned for this key (normally a mahor or a minor scale)
	Scale  Scale
	offset int
}

// KeyScale extends Scale with Notes
type KeyScale struct {
	Scale
	Notes []string
}

// CoFKey represents a Circle Of Fifth Key and adds position and parallel/relative relationships to general key
type CoFKey struct {
	// Key provides general key informations
	Key
	// CirclePos is the position of the key in Circle of Fifths
	CirclePos int
	relative  *Key
	parallel  *Key
}

var cMajor = Key{`C`, Natural, KeySigns[0].Signs, Major, getNotesFromVPanic(SharpType, 0, defaultVisitor), SharpType, ChurchModeScales[0], 0}
var aMinor = Key{`A`, Natural, KeySigns[0].Signs, Minor, getNotesFromVPanic(SharpType, 9, defaultVisitor), SharpType, ChurchModeScales[5], 9}
var gMajor = Key{`G`, Sharp, KeySigns[1].Signs, Major, getNotesFromVPanic(SharpType, 7, defaultVisitor), SharpType, ChurchModeScales[0], 7}
var eMinor = Key{`E`, Sharp, KeySigns[1].Signs, Minor, getNotesFromVPanic(SharpType, 4, defaultVisitor), SharpType, ChurchModeScales[5], 4}
var dMajor = Key{`D`, Sharp, KeySigns[2].Signs, Major, getNotesFromVPanic(SharpType, 2, defaultVisitor), SharpType, ChurchModeScales[0], 2}
var bMinor = Key{`B`, Sharp, KeySigns[2].Signs, Minor, getNotesFromVPanic(SharpType, 11, defaultVisitor), SharpType, ChurchModeScales[5], 11}
var aMajor = Key{`A`, Sharp, KeySigns[3].Signs, Major, getNotesFromVPanic(SharpType, 9, defaultVisitor), SharpType, ChurchModeScales[0], 9}
var fisMinor = Key{`F♯`, Sharp, KeySigns[3].Signs, Minor, getNotesFromVPanic(SharpType, 6, defaultVisitor), SharpType, ChurchModeScales[5], 6}
var eMajor = Key{`E`, Sharp, KeySigns[4].Signs, Major, getNotesFromVPanic(SharpType, 4, defaultVisitor), SharpType, ChurchModeScales[0], 4}
var cisMinor = Key{`C♯`, Sharp, KeySigns[4].Signs, Minor, getNotesFromVPanic(SharpType, 1, defaultVisitor), SharpType, ChurchModeScales[5], 1}
var bMajor = Key{`B`, Sharp, KeySigns[5].Signs, Major, getNotesFromVPanic(SharpType, 11, defaultVisitor), SharpType, ChurchModeScales[0], 11}
var gisMinor = Key{`G♯`, Sharp, KeySigns[5].Signs, Minor, getNotesFromVPanic(SharpType, 8, defaultVisitor), SharpType, ChurchModeScales[5], 8}
var fisMajor = Key{`F♯`, Sharp, KeySigns[6].Signs, Major, getNotesFromVPanic(SharpType, 6, fsharpVisitor), SharpType, ChurchModeScales[0], 6}
var disMinor = Key{`D♯`, Sharp, KeySigns[6].Signs, Minor, getNotesFromVPanic(SharpType, 3, fsharpVisitor), SharpType, ChurchModeScales[5], 3}
var gesMajor = Key{`G♭`, Flat, KeySigns[7].Signs, Major, getNotesFromVPanic(FlatType, 6, gflatVisitor), FlatType, ChurchModeScales[0], 6}
var esMinor = Key{`E♭`, Flat, KeySigns[7].Signs, Minor, getNotesFromVPanic(FlatType, 3, gflatVisitor), FlatType, ChurchModeScales[5], 3}
var desMajor = Key{`D♭`, Flat, KeySigns[8].Signs, Major, getNotesFromVPanic(FlatType, 1, defaultVisitor), FlatType, ChurchModeScales[0], 2}
var bbMinor = Key{`B♭`, Flat, KeySigns[8].Signs, Minor, getNotesFromVPanic(FlatType, 10, defaultVisitor), FlatType, ChurchModeScales[5], 10}
var asMajor = Key{`A♭`, Flat, KeySigns[9].Signs, Major, getNotesFromVPanic(FlatType, 8, defaultVisitor), FlatType, ChurchModeScales[0], 8}
var fmMinor = Key{`F`, Flat, KeySigns[9].Signs, Minor, getNotesFromVPanic(FlatType, 5, defaultVisitor), FlatType, ChurchModeScales[5], 5}
var esMajor = Key{`E♭`, Flat, KeySigns[10].Signs, Major, getNotesFromVPanic(FlatType, 3, defaultVisitor), FlatType, ChurchModeScales[0], 3}
var cmMinor = Key{`C`, Flat, KeySigns[10].Signs, Minor, getNotesFromVPanic(FlatType, 0, defaultVisitor), FlatType, ChurchModeScales[5], 0}
var bbMajor = Key{`B♭`, Flat, KeySigns[11].Signs, Major, getNotesFromVPanic(FlatType, 10, defaultVisitor), FlatType, ChurchModeScales[0], 10}
var gmMinor = Key{`C`, Flat, KeySigns[11].Signs, Minor, getNotesFromVPanic(FlatType, 7, defaultVisitor), FlatType, ChurchModeScales[5], 7}
var fMajor = Key{`F`, Flat, KeySigns[12].Signs, Major, getNotesFromVPanic(FlatType, 5, defaultVisitor), FlatType, ChurchModeScales[0], 5}
var dmMinor = Key{`D`, Flat, KeySigns[12].Signs, Minor, getNotesFromVPanic(FlatType, 2, defaultVisitor), FlatType, ChurchModeScales[5], 2}

var cPtr = &cMajor
var amPtr = &aMinor
var gPtr = &gMajor
var emPtr = &eMinor
var dPtr = &dMajor
var bmPtr = &bMinor
var aPtr = &aMajor
var fismPtr = &fisMinor
var ePtr = &eMajor
var cismPtr = &cisMinor
var bPtr = &bMajor
var gismPtr = &gisMinor
var fisPtr = &fisMajor
var dismPtr = &disMinor
var gesPtr = &gesMajor
var esmPtr = &esMinor
var desPtr = &desMajor
var bbmPtr = &bbMinor
var asPtr = &asMajor
var fmPtr = &fmMinor
var esPtr = &esMajor
var cmPtr = &cmMinor
var bbPtr = &bMajor
var gmPtr = &gmMinor
var fPtr = &fMajor
var dmPtr = &dmMinor

// CoFKeys lists the Keys of a Circle Of Fifth (reduced to max 6 signs). F-Sharp and Gb-Flat are listed both
var CoFKeys = []CoFKey{
	{cMajor, 0, amPtr, cmPtr},
	{aMinor, 0, cPtr, aPtr},
	{gMajor, 1, emPtr, gmPtr},
	{eMinor, 1, gPtr, ePtr},
	{dMajor, 2, bmPtr, dmPtr},
	{bMinor, 2, dPtr, bPtr},
	{aMajor, 3, fismPtr, amPtr},
	{fisMinor, 3, aPtr, fisPtr},
	{eMajor, 4, cismPtr, emPtr},
	{cisMinor, 4, ePtr, desPtr},
	{bMajor, 5, gismPtr, bmPtr},
	{gisMinor, 5, bPtr, asPtr},
	{fisMajor, 6, dismPtr, fismPtr},
	{disMinor, 6, fisPtr, esPtr},
	{gesMajor, 6, esmPtr, fismPtr},
	{esMinor, 6, gesPtr, esPtr},
	{desMajor, 7, bbmPtr, cismPtr},
	{bbMinor, 7, desPtr, bbPtr},
	{asMajor, 8, fmPtr, gismPtr},
	{fmMinor, 8, asPtr, fPtr},
	{esMajor, 9, cmPtr, esmPtr},
	{cmMinor, 9, esPtr, cPtr},
	{bbMajor, 10, gmPtr, bbmPtr},
	{gmMinor, 10, bbPtr, gPtr},
	{fMajor, 11, dmPtr, fmPtr},
	{dmMinor, 11, fPtr, dPtr},
}

func getMajorKeyByPosition(pos int) CoFKey {
	if pos > 11 {
		pos = pos - 12
	}
	for _, v := range CoFKeys {
		if v.TonalType == Major && v.CirclePos == pos {
			return v
		}
	}
	return CoFKeys[0]
}

func defaultVisitor(i int, n string) string {
	return n
}

func fsharpVisitor(i int, n string) string {
	if n == `F` {
		return `E♯`
	}
	return n
}

func gflatVisitor(i int, n string) string {
	if n == `B` {
		return `C♭`
	}
	return n
}

// GetParallel returns the key with the same tonic but opposite tonality (e.g. for C-Major C-Minor is returned)
func (k CoFKey) GetParallel() Key {
	return *k.parallel
}

// GetRelative returns the relative key (in case of major the minor key and vice versa)
func (k CoFKey) GetRelative() Key {
	return *k.relative
}

// MinorList stores the 3 minor types
type MinorList struct {
	Natural  []string
	Harmonic []string
	Melodic  []string
}

// MinorListRP stores MinorLists for relative and parallel key
type MinorListRP struct {
	Relative MinorList
	Parallel MinorList
}

// ChurchModeScaleList is a list of all churchmode scales
type ChurchModeScaleList struct {
	IonianScale     []string
	DorianScale     []string
	PhygrianScale   []string
	LydianScale     []string
	MixolydianScale []string
	AeolianScale    []string
	LocrianScale    []string
}

// CMSListRP holds the churchmode scales for the own or parallel key and the relative key
type CMSListRP struct {
	Relative ChurchModeScaleList
	Parallel ChurchModeScaleList
}

func majorGetMinors(k CoFKey) MinorListRP {
	offsetR := k.GetRelative().offset
	ntR := k.NoteType
	notesRN, _ := GetMinorScale(NaturalMinor, ntR, offsetR)
	notesRH, _ := GetMinorScale(HarmonicMinor, ntR, offsetR)
	notesRM, _ := GetMinorScale(MelodicMinor, ntR, offsetR)

	offsetP := k.GetParallel().offset
	ntP := k.GetParallel().NoteType
	notesPN, _ := GetMinorScale(NaturalMinor, ntP, offsetP)
	notesPH, _ := GetMinorScale(HarmonicMinor, ntP, offsetP)
	notesPM, _ := GetMinorScale(MelodicMinor, ntP, offsetP)

	return MinorListRP{
		Relative: MinorList{
			notesRN,
			notesRH,
			notesRM,
		},
		Parallel: MinorList{
			notesPN,
			notesPH,
			notesPM,
		},
	}
}

func minorGetMinors(k CoFKey) MinorListRP {
	offsetR := k.offset
	ntR := k.NoteType
	notesRN, _ := GetMinorScale(NaturalMinor, ntR, offsetR)
	notesRH, _ := GetMinorScale(HarmonicMinor, ntR, offsetR)
	notesRM, _ := GetMinorScale(MelodicMinor, ntR, offsetR)

	offsetP := k.GetParallel().offset + 9
	if offsetP > 11 {
		offsetP = offsetP - 12
	}
	ntP := k.GetParallel().NoteType
	notesPN, _ := GetMinorScale(NaturalMinor, ntP, offsetP)
	notesPH, _ := GetMinorScale(HarmonicMinor, ntP, offsetP)
	notesPM, _ := GetMinorScale(MelodicMinor, ntP, offsetP)

	return MinorListRP{
		Relative: MinorList{
			notesRN,
			notesRH,
			notesRM,
		},
		Parallel: MinorList{
			notesPN,
			notesPH,
			notesPM,
		},
	}
}

// GetMinorScales returns all minorscales for that key and the parallel key
func (k CoFKey) GetMinorScales() MinorListRP {
	if k.TonalType == Major {
		return majorGetMinors(k)
	}
	return minorGetMinors(k)
}

func majorGetCMS(k CoFKey) CMSListRP {
	var cmslrp CMSListRP
	var err error

	cmslrp.Relative.IonianScale, err = GetChurchModeScaleNotes(Ionian, k.Notes)
	cmslrp.Relative.DorianScale, err = GetChurchModeScaleNotes(Dorian, k.Notes)
	cmslrp.Relative.PhygrianScale, err = GetChurchModeScaleNotes(Phygrian, k.Notes)
	cmslrp.Relative.LydianScale, err = GetChurchModeScaleNotes(Lydian, k.Notes)
	cmslrp.Relative.MixolydianScale, err = GetChurchModeScaleNotes(Mixolydian, k.Notes)
	cmslrp.Relative.AeolianScale, err = GetChurchModeScaleNotes(Aeolian, k.Notes)
	cmslrp.Relative.LocrianScale, err = GetChurchModeScaleNotes(Locrian, k.Notes)

	cmslrp.Parallel.IonianScale, err = GetChurchModeScaleNotes(Ionian, k.Notes)
	cmslrp.Parallel.DorianScale, err = GetChurchModeScaleNotes(Dorian, getMajorKeyByPosition(k.CirclePos+10).Notes)
	cmslrp.Parallel.PhygrianScale, err = GetChurchModeScaleNotes(Phygrian, getMajorKeyByPosition(k.CirclePos+8).Notes)
	cmslrp.Parallel.LydianScale, err = GetChurchModeScaleNotes(Lydian, getMajorKeyByPosition(k.CirclePos+1).Notes)
	cmslrp.Parallel.MixolydianScale, err = GetChurchModeScaleNotes(Mixolydian, getMajorKeyByPosition(k.CirclePos+11).Notes)
	cmslrp.Parallel.AeolianScale, err = GetChurchModeScaleNotes(Aeolian, getMajorKeyByPosition(k.CirclePos+9).Notes)
	cmslrp.Parallel.LocrianScale, err = GetChurchModeScaleNotes(Locrian, getMajorKeyByPosition(k.CirclePos+7).Notes)

	if err != nil {
		panic(err)
	}
	return cmslrp
}

func minorGetCMS(k CoFKey) CMSListRP {
	var cmslrp CMSListRP
	var err error
	rel := k.GetRelative()

	cmslrp.Relative.IonianScale, err = GetChurchModeScaleNotes(Ionian, rel.Notes)
	cmslrp.Relative.DorianScale, err = GetChurchModeScaleNotes(Dorian, rel.Notes)
	cmslrp.Relative.PhygrianScale, err = GetChurchModeScaleNotes(Phygrian, rel.Notes)
	cmslrp.Relative.LydianScale, err = GetChurchModeScaleNotes(Lydian, rel.Notes)
	cmslrp.Relative.MixolydianScale, err = GetChurchModeScaleNotes(Mixolydian, rel.Notes)
	cmslrp.Relative.AeolianScale, err = GetChurchModeScaleNotes(Aeolian, rel.Notes)
	cmslrp.Relative.LocrianScale, err = GetChurchModeScaleNotes(Locrian, rel.Notes)

	cmslrp.Parallel = majorGetCMS(getMajorKeyByPosition(k.CirclePos)).Parallel

	if err != nil {
		panic(err)
	}
	return cmslrp
}

// GetCMS returns all churchmodescales for this key and the parallel key
func (k CoFKey) GetCMS() CMSListRP {
	if k.TonalType == Major {
		return majorGetCMS(k)
	}
	return minorGetCMS(k)
}

// GetScaleInKey returns for a given scale (formula) the corresponding notes
func (k CoFKey) GetScaleInKey(s Scale) (KeyScale, error) {
	key := k.Key
	var res KeyScale
	var err error
	if k.TonalType == Minor {
		key = k.GetRelative()
	}
	res.Notes, err = s.GetScaleNotes(key.Notes)
	res.Scale = s
	return res, err
}

func roman(i int) string {
	const c = '\u2160'
	return string(rune(c + int64(i)))
}

// PrettyPrint implements the IPrettyPrint interface for KeyScale
func (ks KeyScale) PrettyPrint() string {
	const c = '\u2160'
	/*
		row1 := `|`
		row2 := `|`
		row3 := `|`
		row4 := `|`
		p1 := `Name: **` + ks.Name + `**`
		for i, v := range ks.Formula {
			row1 += string(rune(c+i)) + `|`
			row2 += `:-:|`
			row3 += v + `|`
			row4 += ks.Notes[i] + `|`
		}

		return p1 + "\n\n" + row1 + "\n" + row2 + "\n" + row3 + "\n" + row4
	*/
	var tpl bytes.Buffer

	data := struct {
		Name    string
		Header  func(i int) string
		Formula []string
		Notes   []string
		TS      []string
		Type    ScaleType
	}{
		ks.Name,
		roman,
		ks.Formula,
		ks.Notes,
		ks.GetSTFormula(),
		ks.Type,
	}
	t, err := template.New("scale").Parse(`
Name: **{{ .Name }}**

Type: **{{.Type}}**

|{{range $index,$val := .Formula}}{{call $.Header $index}}|{{end}}
|{{range $index,$val := .Formula}}:-:|{{end}}
|{{range $index,$val := .Formula}}{{.}}|{{end}}
|{{range $index,$val := .Notes}}{{.}}|{{end}}`)

	err = t.Execute(&tpl, data)
	if err != nil {
		panic(err)
	}
	return tpl.String()
}
