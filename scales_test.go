package musictheory

import (
	"fmt"
	"testing"
)

func TestGetStepString(t *testing.T) {
	if getStepString(3) != `` {
		t.Errorf("expected empty string but got %v", getStepString(3))
	}
}

func ExampleScale_GetSTFormula() {
	b := ChurchModeScales[0].GetSTFormula()
	fmt.Println(b)
	b = ChurchModeScales[5].GetSTFormula()
	fmt.Println(b)
	// Output:
	// [T T S T T T S]
	// [T S T T S T T]
}

func ExampleChurchMode_String() {
	var cm ChurchMode = 23
	fmt.Printf("Dorian: %v, %s\n", Dorian, Dorian)
	fmt.Printf("cm: %v, %s", cm, cm)
	// Output:
	// Dorian: Dorian, Dorian
	// cm: 23, 23
}

func TestScale_GetScaleNotes(t *testing.T) {
	s := Scale{
		`TestScale`,
		[]string{`x`, `x`},
		Pentatonic,
	}
	n, _ := GetNotesFrom(SharpType, 0)
	_, err := s.GetScaleNotes(n)
	if err == nil {
		t.Error("exptected error but got none")
	}
}

func ExampleScale_GetScaleNotes() {
	minor := ChurchModeScales[5]
	eMinorNotes, _ := minor.GetScaleNotes(CoFKeys[3].Notes)
	majPenta := Scale{`Major Pentatonic`, []string{`1`, `2`, `3`, `5`, `6`}, Pentatonic}
	pentaNotes, _ := majPenta.GetScaleNotes(CoFKeys[2].Notes)
	fmt.Println(eMinorNotes)
	fmt.Println(pentaNotes)
	// Output:
	// [E F♯ G A B C D]
	// [G A B D E]
}

func ExampleGetChurchModeScaleNotes() {
	dorian, _ := GetChurchModeScaleNotes(Dorian, CoFKeys[4].Notes)
	phygrian, _ := GetChurchModeScaleNotes(Phygrian, CoFKeys[4].Notes)
	fmt.Println(dorian)
	fmt.Println(phygrian)
	// Output:
	// [E F♯ G A B C♯ D]
	// [F♯ G A B C♯ D E]
}

func ExampleGetMinorScale() {
	fmt.Println("--- D-Minor ---")
	notes, err := GetMinorScale(NaturalMinor, FlatType, 2)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(notes)
	notes, _ = GetMinorScale(HarmonicMinor, FlatType, 2)
	fmt.Println(notes)
	notes, _ = GetMinorScale(MelodicMinor, FlatType, 2)
	fmt.Println(notes)
	fmt.Println("--- A-Minor ---")
	notes, _ = GetMinorScale(NaturalMinor, SharpType, 9)
	fmt.Println(notes)
	notes, _ = GetMinorScale(HarmonicMinor, SharpType, 9)
	fmt.Println(notes)
	notes, _ = GetMinorScale(MelodicMinor, SharpType, 9)
	fmt.Println(notes)
	fmt.Println("--- G♯-Minor ---")
	notes, _ = GetMinorScale(NaturalMinor, SharpType, 8)
	fmt.Println(notes)
	notes, _ = GetMinorScale(HarmonicMinor, SharpType, 8)
	fmt.Println(notes)
	notes, _ = GetMinorScale(MelodicMinor, SharpType, 8)
	fmt.Println(notes)
	fmt.Println("--- F-Minor ---")
	notes, _ = GetMinorScale(NaturalMinor, FlatType, 5)
	fmt.Println(notes)
	notes, _ = GetMinorScale(HarmonicMinor, FlatType, 5)
	fmt.Println(notes)
	notes, _ = GetMinorScale(MelodicMinor, FlatType, 5)
	fmt.Println(notes)
	// Output:
	// 	--- D-Minor ---
	// [D E F G A B♭ C]
	// [D E F G A B♭ C♯]
	// [D E F G A B C♯]
	// --- A-Minor ---
	// [A B C D E F G]
	// [A B C D E F G♯]
	// [A B C D E F♯ G♯]
	// --- G♯-Minor ---
	// [G♯ A♯ B C♯ D♯ E F♯]
	// [G♯ A♯ B C♯ D♯ E F𝄪]
	// [G♯ A♯ B C♯ D♯ E♯ F𝄪]
	// --- F-Minor ---
	// [F G A♭ B♭ C D♭ E♭]
	// [F G A♭ B♭ C D♭ E]
	// [F G A♭ B♭ C D E]
}

func TestGetMinorScale(t *testing.T) {
	var mt MinorType
	mt = 23
	func() {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("TestUserFail should have panicked!")
			}
		}()
		_, err := GetMinorScale(mt, SharpType, 23)
		if err == nil {
			t.Error("error expected")
		}
	}()
}
