# Goal Coverage

## Basics

- [x] Names, Declarations, Variables, Identifiers
- [x] Pointers
- [x] Assignments, make, new
- [x] if, range, switch

## Data Types

- [x] Integers
- [x] Booleans
- [x] Strings
- [x] Constants
- [x] Runes (int64)
- [ ] Arrays
- [x] Slices
- [x] Maps
- [x] Structs
- [x] Templates

## Functions

- [x] Function Declaration
- [ ] Recursion
- [x] Multiple Return Values
- [x] Errors
- [x] Anonymous Functions
- [ ] Variadic Functions
- [x] Deferred Function Calls, Recover, Panic

## Methods

- [x] Method Declaration
- [ ] Method with Pointer Receiver

## Interfaces

- [x] Interfaces as Contracts
- [x] Interface Types
- [x] Interface Satisfaction

## Goroutines, Concurrency, Channels

- [ ] All

## Packages

- [x] Import Path
- [x] Import Declaration
- [x] Package Declaration

## Testing

- [x] Testfunction
- [x] Examplefunction
- [x] Coverage and Coverageanalysis

## Documentation

- [x] go doc
- [x] godoc

