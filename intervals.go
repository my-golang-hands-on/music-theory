package musictheory

import "errors"

// Step is the base type for Step enumeration
type Step int

const (
	// Semitone is the smallest interval (used here) aka halftone, halfstep
	Semitone Step = iota + 1
	// Tone consists of tow semitones aka wholetone, fullstep
	Tone
)

// ISymbol defines an Interval Info
type ISymbol struct {
	// Symbol of the interval e.g. 1, b3, ...
	Symbol string
	// Name is the descriptive name of the interval
	Name string
	// Cipher is the position in a 12 note array the interval represents
	Cipher int
}

// GetSemis returns the distance in semitones between to interval symbols
func (i ISymbol) GetSemis(o string) (int, error) {
	other, e := Intervals.GetIntervalBySymbol(o)
	if e == nil {
		return other.Cipher - i.Cipher, nil
	}
	return 0, e
}

// ISymbols represents a type to store a list of Intervals
type ISymbols []ISymbol

// Intervals lists all intervals as a list of Symbol Types
var Intervals = ISymbols{
	{`1`, `Perfect unison`, 0},
	{`♭2`, `Minor second`, 1},
	{`2`, `Major second`, 2},
	{`♭3`, `Minor third`, 3},
	{`3`, `Major third`, 4},
	{`4`, `Perfect fourth`, 5},
	{`♯4`, `Augmented fourth`, 6},
	{`♭5`, `Diminished fifth`, 6},
	{`5`, `Perfect fifth`, 7},
	{`♯5`, `Augmented fifth`, 8},
	{`♭6`, `Minor sixth`, 8},
	{`6`, `Major sixth`, 9},
	{`♭7`, `Minor seventh`, 10},
	{`7`, `Major seventh`, 11},
	{`8`, `Perfect octave`, 12},
}

// GetIntervalBySymbol returns a Symbol struct from the Intervals List corresponding to the given symbol-string.
// If the symbol can not be found an error is returned
func (s ISymbols) GetIntervalBySymbol(sym string) (ISymbol, error) {
	return getIntervalBySearch(s, sym)
}

// GetIntervalByCipher returns a Symbol struct from the Intervals List corresponding to the given cypher
// If the symbol can not be found an error is returned
func (s ISymbols) GetIntervalByCipher(c int) (ISymbol, error) {
	return getIntervalBySearch(s, c)
}

func getIntervalBySearch(s ISymbols, se interface{}) (ISymbol, error) {
	for _, v := range s {
		switch se.(type) {
		case string:
			if se == v.Symbol {
				return v, nil
			}
		case int:
			if se == v.Cipher {
				return v, nil
			}
		default:
			return ISymbol{}, errors.New("GetIntervalBySymbol: symbol not found")
		}
	}
	return ISymbol{}, errors.New("GetIntervalBySymbol: symbol not found")
}
